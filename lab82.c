#include <string.h>
#include <stdio.h>

#define MAXLEN 1000
#define LINELEN 20

int main(int argc, char *argv[])
{
    char line[LINELEN];
    char str[MAXLEN] = "\0";

    for (int i = 1; i < argc; i++)
    {
        strcpy(line, argv[i]);

        if (strstr(str, line) == NULL)
        {
            if (strcmp(line, str) < 0)
            {
                strcat(line, " ");
                size_t len = strlen(line);
                memmove(str + len, str, strlen(str) + 1);
                memcpy(str, line, len);
            }
            else
            {
                strcat(str, line);
                strcat(str, " ");
            }
        }
        printf("%s\n", str);
    }
}

