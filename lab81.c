#include <string.h>
#include <stdio.h>

#define MAXLEN 1000
#define LINELEN 20

int main()
{
    char line[LINELEN];
    char str[MAXLEN] = "\0";
    while(1)
    {
        printf("Enter a string: ");
        fgets(line, LINELEN, stdin);
        *strchr(line, '\n') = '\0';

        if (strcmp(line, "DONE") == 0)
        {
            printf("%s\n", str);
            break;
        }

        if (strstr(str, line) == NULL)
        {
            if (strcmp(line, str) < 0)
            {
                strcat(line, " ");
                size_t len = strlen(line);
                memmove(str + len, str, strlen(str) + 1);
                memcpy(str, line, len);
            }
            else
            {
                strcat(str, line);
                strcat(str, " ");
            }
        }
        printf("%s\n", str);
    }
}

